$("#price_discount").on('input', function(e){
    $(this).val(formatCurrency(this.value.replace(/[.]/g,'')));
}).on('keypress',function(e){
    if(!$.isNumeric(String.fromCharCode(e.which))) e.preventDefault();
}).on('paste', function(e){
    var cb = e.originalEvent.clipboardData || window.clipboardData;
    if(!$.isNumeric(cb.getData('text'))) e.preventDefault();
});

$("#min_order_value").on('input', function(e){
    $(this).val(formatCurrency(this.value.replace(/[.]/g,'')));
}).on('keypress',function(e){
    if(!$.isNumeric(String.fromCharCode(e.which))) e.preventDefault();
}).on('paste', function(e){
    var cb = e.originalEvent.clipboardData || window.clipboardData;
    if(!$.isNumeric(cb.getData('text'))) e.preventDefault();
});
$(function() {
    var price_discount = $("#price_discount").val();
    var min_order_value = $("#min_order_value").val();
    price_discount = formatCurrency(price_discount.replace(/[.]/g,''));
    min_order_value = formatCurrency(min_order_value.replace(/[.]/g,''));
    $("#price_discount").val(price_discount);
    $("#min_order_value").val(min_order_value);
});
