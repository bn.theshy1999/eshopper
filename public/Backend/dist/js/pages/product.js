$(function () {
    //Initialize Select2 Elements
    $('.select2').select2({
        theme: 'bootstrap4'
    });
    var price = $("#price").val();
    price = formatCurrency(price.replace(/[.]/g,''));
    $("#price").val(price);
    var price_fake = $("#price_fake").val();
    price_fake = formatCurrency(price_fake.replace(/[.]/g,''));
    $("#price_fake").val(price_fake);
    $("input[data-bootstrap-switch]").each(function () {
        $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });
    $('#image').each(function () {
        var $input = $(this);

        $input.on('change', function (element) {
            var fileName = '';
            if (element.target.value) fileName = element.target.value.split('\\').pop();
            $('.file-name-image').text(' ' + fileName)
        });
    });
    CKEDITOR.config.allowedContent = true;
    CKEDITOR.replace('desc_info');
    CKEDITOR.replace('desc');
});

function preview_image(event) {
    var reader = new FileReader();
    reader.onload = function () {
        var output = document.getElementById('image-pre');
        output.src = reader.result;
    }
    reader.readAsDataURL(event.target.files[0]);
}
$("#price, #price_fake").on('input', function(e){
    $(this).val(formatCurrency(this.value.replace(/[.]/g,'')));
}).on('keypress',function(e){
    if(!$.isNumeric(String.fromCharCode(e.which))) e.preventDefault();
}).on('paste', function(e){
    var cb = e.originalEvent.clipboardData || window.clipboardData;
    if(!$.isNumeric(cb.getData('text'))) e.preventDefault();
});