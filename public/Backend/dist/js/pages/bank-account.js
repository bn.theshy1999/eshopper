$(function () {
    $("input[data-bootstrap-switch]").each(function () {
        $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });
    $('#image').each(function () {
        var $input = $(this);

        $input.on('change', function (element) {
            var fileName = '';
            if (element.target.value) fileName = element.target.value.split('\\').pop();
            $('.file-name-image').text(' ' + fileName)
        });
    });
});

function preview_image_1(event) {
    var reader = new FileReader();
    reader.onload = function(){
        var output = document.getElementById('image-pre-1');
        output.src = reader.result;
    }
    reader.readAsDataURL(event.target.files[0]);
}

function preview_image_2(event) {
    var reader = new FileReader();
    reader.onload = function(){
        var output = document.getElementById('image-pre-2');
        output.src = reader.result;
    }
    reader.readAsDataURL(event.target.files[0]);
}
