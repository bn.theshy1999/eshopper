<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class CategoryControllerList extends Controller
{
    public function category($slug,$CategoryID)
    {
        $categoryMenu = Category::GetCategoryParent()->where('status',1)->take(5)->get();
        $category = Category::GetCategoryParent()->where('status',1)->get();
        $product = Product::where('category_id',$CategoryID)->paginate(12);
        return view('Frontend.product.category.list',compact('category','categoryMenu','product'));
    }
}
