<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\ProductImage;
use App\Traits\StorageImageTrait;
use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;
use App\Models\Product;
use App\Models\Category;
use App\Traits\LoggerTrait;
use App\Models\Tag;

class ProductController extends Controller
{
    use LoggerTrait;
    use StorageImageTrait;
    public function index(Request $request)
    {
        try {
            $name = $request->get('name', '');
            $query = Product::with(['category']);
            if ($name) {
                $query->where('name', 'like', '%' . $name . '%');
            }
            $datas = $query->orderBy('id', 'DESC')->paginate(10);
            return view('Backend.product.index',compact(['datas','name']));
        } catch (\Exception $exception) {
            $this->logError($exception);
            return redirect()->back()->with('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
    }


    public function create()
    {
        try {
            $listCategory = Category::GetCategoryParent()->with(['children' => function ($query) { $query->where('deleted', 0); }])->get();
            return view('Backend.product.form', compact('listCategory'));
        } catch (\Exception $exception) {
            $this->logError($exception);
            return redirect()->back()->with('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
    }

    public function store(ProductRequest $request)
    {
        try {
            $datas = [
                'name' => $request->name,
                'price' => $request->price,
                'content' => $request->contents,
                'user_id' => auth()->id(),
                'category_id' => $request->category_id
            ];
            $dataUpload = $this->storageTraitUpload($request,'feature_image_path','product');
            if(!empty($dataUpload)){
                $datas['feature_image_path'] = $dataUpload['file_path'];
                $datas['feature_image_name'] = $dataUpload['file_name'];
            }
            $product = Product::create($datas);

            // Inser data to product_image
            if($request->hasFile('image_path')){
                foreach ($request->image_path as $fileItem){
                    $dataProductImage = $this->storageTraitUploadMutiple($fileItem, 'product');
                    $productImage = ProductImage::create([
                        'image_path' => $dataProductImage['file_path'],
                        'image_name' => $dataProductImage['file_path'],
                        'product_id' => $product->id
                    ]);
                }
            }

            //Insert data to tags
            if(!empty($request->tags)){
                foreach ($request->tags as $tagItem)
                {
                    $tagInstance = Tag::firstOrCreate(['name' => $tagItem]);
                    $tagID[] = $tagInstance->id;
                }
                $product->tags()->attach($tagID);
            }
            return redirect()->route('admin.product.index')->with('success', 'Thêm mới thành công');
        } catch (\Exception $exception) {
            $this->logError($exception);
            return redirect()->back()->withInput()->with('error', 'Thêm mới thất bại');
        }
    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        try {
            $product = Product::findOrFail($id);
            $listCategory = Category::GetCategoryParent()->with(['children' => function ($query) { $query->where('deleted', 0); }])->get();
            return view('Backend.product.form', compact('listCategory','product'));
        } catch (\Exception $exception) {
            $this->logError($exception);
            return redirect()->back()->with('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
    }


    public function update(ProductRequest $request, $id)
    {
        try {
            $datas = [
                'name' => $request->name,
                'price' => $request->price,
                'content' => $request->contents,
                'user_id' => auth()->id(),
                'category_id' => $request->category_id
            ];
            $dataUpload = $this->storageTraitUpload($request,'feature_image_path','product');
            if(!empty($dataUpload)){
                $datas['feature_image_path'] = $dataUpload['file_path'];
                $datas['feature_image_name'] = $dataUpload['file_name'];
            }
            Product::find($id)->update($datas);
            $product = Product::find($id);
            // Inser data to product_image
            if($request->hasFile('image_path')){
                ProductImage::where('product_id',$id)->delete();
                foreach ($request->image_path as $fileItem){
                    $dataProductImage = $this->storageTraitUploadMutiple($fileItem, 'product');
                    $productImage = ProductImage::firstOrCreate([
                        'image_path' => $dataProductImage['file_path'],
                        'image_name' => $dataProductImage['file_path'],
                        'product_id' => $product->id
                    ]);
                }
            }

            //Insert data to tags
            if(!empty($request->tags)){
                foreach ($request->tags as $tagItem)
                {
                    $tagInstance = Tag::firstOrCreate(['name' => $tagItem]);
                    $tagID[] = $tagInstance->id;
                }
                $product->tags()->sync($tagID);
            }
            return redirect()->route('admin.product.index')->with('success', 'Chỉnh sửa thành công');
        } catch (\Exception $exception) {
            $this->logError($exception);
            return redirect()->back()->withInput()->with('error', 'Chỉnh sửa thất bại');
        }
    }


    public function destroy($id)
    {
        try {
            Product::find($id)->delete();
            return redirect()->route('admin.product.index')->with('success','Xóa thành công');
        } catch (\Exception $exception) {
            $this->logError($exception);
            return redirect()->back()->withInput()->with('error', 'xóa thất bại');
        }
    }
}
