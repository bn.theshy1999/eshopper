<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\SliderRequest;
use App\Models\Slider;
use App\Traits\LoggerTrait;
use App\Traits\StorageImageTrait;

class SliderController extends Controller
{
    use LoggerTrait;
    use StorageImageTrait;
    public function index(Request $request)
    {
        try {
//            $name = $request->get('name', '');
//            $query = Slider::with(['category']);
//            if ($name) {
//                $query->where('name', 'like', '%' . $name . '%');
//            }
            $datas = Slider::orderBy('id', 'DESC')->paginate(10);
            return view('Backend.slider.index',compact('datas'));
        } catch (\Exception $exception) {
            $this->logError($exception);
            return redirect()->back()->with('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
    }


    public function create()
    {
        try {
            return view('backend.slider.form');
        } catch (\Exception $exception) {
            $this->logError($exception);
            return redirect()->back()->with('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
    }

    public function store(SliderRequest $request)
    {
        try {
            $datas = [
                'name' => $request->name,
                'description' => $request->description,
            ];
            $dataUpload = $this->storageTraitUpload($request,'image_path','slider');
            if(!empty($dataUpload)){
                $datas['image_path'] = $dataUpload['file_path'];
                $datas['image_name'] = $dataUpload['file_name'];
            }
            $slider = Slider::create($datas);
            return redirect()->route('admin.slider.index')->with('success', 'Thêm mới thành công');
        } catch (\Exception $exception) {
            $this->logError($exception);
            return redirect()->back()->with('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
    }


    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        try {
            $slider = Slider::findOrFail($id);
            return view('Backend.slider.form', compact('slider'));
        } catch (\Exception $exception) {
            $this->logError($exception);
            return redirect()->back()->with('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
    }

    public function update(SliderRequest $request, $id)
    {
        try {
            $datas = [
                'name' => $request->name,
                'description' => $request->description,
            ];
            $dataUpload = $this->storageTraitUpload($request,'image_path','slider');
            if(!empty($dataUpload)){
                $datas['image_path'] = $dataUpload['file_path'];
                $datas['image_name'] = $dataUpload['file_name'];
            }
            $slider = Slider::find($id)->update($datas);
            return redirect()->route('admin.slider.index')->with('success', 'Chỉnh sửa thành công');
        } catch (\Exception $exception) {
            $this->logError($exception);
            return redirect()->back()->with('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
    }

    public function destroy($id)
    {
        try {
            Slider::find($id)->delete();
            return redirect()->route('admin.slider.index')->with('success','Xóa thành công');
        } catch (\Exception $exception) {
            $this->logError($exception);
            return redirect()->back()->withInput()->with('error', 'xóa thất bại');
        }
    }
}
