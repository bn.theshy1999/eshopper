<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;
use App\Traits\LoggerTrait;
use App\Http\Requests\SettingRequest;

class SettingController extends Controller
{
    use LoggerTrait;

    public function index()
    {
        try {
//            $name = $request->get('name', '');
//            $query = Slider::with(['category']);
//            if ($name) {
//                $query->where('name', 'like', '%' . $name . '%');
//            }
            $datas = Setting::orderBy('id', 'DESC')->paginate(10);
            return view('backend.setting.index', compact('datas'));

        } catch (\Exception $exception) {
            $this->logError($exception);
            return redirect()->back()->with('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
    }


    public function create()
    {
        try {
            return view('backend.setting.form');

        } catch (\Exception $exception) {
            $this->logError($exception);
            return redirect()->back()->with('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
    }

    public function store(SettingRequest $request)
    {
        try {
            $datas = [
                'config_key' => $request->config_key,
                'config_value' => $request->config_value,
                'type' => $request->type,
            ];
            $slider = Setting::create($datas);
            return redirect()->route('admin.setting.index')->with('Thêm mới thành công');
        } catch (\Exception $exception) {
            $this->logError($exception);
            return redirect()->back()->with('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
    }


    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $setting = Setting::findOrFail($id);
            return view('Backend.setting.form', compact('setting'));
        } catch (\Exception $exception) {
            $this->logError($exception);
            return redirect()->back()->with('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Setting::find($id)->delete();
            return redirect()->route('admin.setting.index')->with('success','Xóa thành công');
        } catch (\Exception $exception) {
            $this->logError($exception);
            return redirect()->back()->withInput()->with('error', 'xóa thất bại');
        }
    }
}
