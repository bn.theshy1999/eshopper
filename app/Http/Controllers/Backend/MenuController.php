<?php

namespace App\Http\Controllers\Backend;

use App\Components\MenuRecusive;
use App\Http\Controllers\Controller;
use App\Http\Requests\MenuRequest;
use Illuminate\Http\Request;
use App\Models\Menu;
use App\Traits\LoggerTrait;
use App\Helpers\Backend\Helpers;



class MenuController extends Controller
{
    use LoggerTrait;
    private $menuRecusive;
    private $menu;
    public function __construct(MenuRecusive $menuRecusive, Menu $menu)
    {
        $this->menuRecusive = $menuRecusive;
        $this->menu = $menu;
    }
    public function index(Request $request)
    {
        try {
            $name = $request->get('name', '');
            $query = Menu::GetMenu();
            if ($name) {
                $query->where('name', 'like', '%' . $name . '%');
            }
            $menus = $query->orderBy('id', 'DESC')->paginate(20);
            $menusindex = $this->menuRecusive->menuRecusiveindex();
            return view('Backend.menu.index', compact('menus', 'name','menusindex'));
        } catch (\Exception $exception) {
            $this->logError($exception);
            return redirect()->back()->with('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
    }

    public function create()
    {
        try {
            $listMenu = $this->menuRecusive->menuRecusiveAdd();
            return view('backend.menu.form',compact('listMenu'));
        } catch (\Exception $exception) {
            $this->logError($exception);
            return redirect()->back()->with('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
    }


    public function store(MenuRequest $request)
    {
        try {
            $this->menu->create([
                'name' => $request->name,
                'parent_id' => $request->parent_id,
                'slug' => Helpers::createSlug($request->name)
            ]);
            return redirect()->route('admin.menu.index')->with('success','Thêm thành công');
        } catch (\Exception $exception) {
            $this->logError($exception);
            return redirect()->back()->with('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        try {
            $menu = $this->menu->find($id);
            $listMenu = $this->menuRecusive->menuRecusiveEdit($menu->parent_id);
            return view('backend.menu.form',compact('listMenu','menu'));
        } catch (\Exception $exception) {
            $this->logError($exception);
            return redirect()->back()->with('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
    }


    public function update(MenuRequest $request, $id)
    {
        try {
            $this->menu->find($id)->update([
                'name' => $request->name,
                'parent_id' => $request->parent_id,
                'slug' => Helpers::createSlug($request->name)
            ]);
            return redirect()->route('admin.menu.index')->with('success','Sửa thành công');
        } catch (\Exception $exception) {
            $this->logError($exception);
            return redirect()->back()->with('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
    }

    public function destroy($id)
    {
        try {
            $this->menu->find($id)->delete();
            return redirect()->route('admin.menu.index')->with('success','Xóa thành công');
        } catch (\Exception $exception) {
            $this->logError($exception);
            return redirect()->back()->withInput()->with('error', 'xóa thất bại');
        }
    }
}
