<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Traits\LoggerTrait;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Http\Requests\CategoryRequest;
use App\Helpers\Backend\Helpers;


class CategoryController extends Controller
{
    use LoggerTrait;

    public function index(Request $request)
    {
        try {
            $name = $request->get('name', '');
            $query = Category::GetCategory();
            if ($name) {
                $query->where('name', 'like', '%' . $name . '%');
            }
            $datas = $query->orderBy('id', 'DESC')->paginate(20);
            return view('Backend.category.index', compact('datas', 'name'));
        } catch (\Exception $exception) {
            $this->logError($exception);
            return redirect()->back()->with('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
    }

    public function create()
    {
        try {
            $listCategory = Category::GetCategoryParent()->get();
            return view('Backend.category.form', compact('listCategory'));
        } catch (\Exception $exception) {
            $this->logError($exception);
            return redirect()->back()->with('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
    }


    public function store(CategoryRequest $request)
    {
        try {
            $datas = $request->all();
            $datas['status'] = $request->has('status') ? Category::ACTIVE : Category::INACTIVE;
            $datas['slug'] = Helpers::createSlug($datas['name']);
            Category::create($datas);
            return redirect()->route('admin.category.index')->with('success', 'Thêm mới thành công');
        } catch (\Exception $exception) {
            $this->logError($exception);
            return redirect()->back()->withInput()->with('error', 'Thêm mới thất bại');
        }
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        try {
            $listCategory = Category::GetCategoryParent()->where('id', '!=', $id)->get();// không cho thằng cha gọi đến chính nó
            $category = Category::where('id', $id)->first();
            return $category ? view('backend.category.form', compact('listCategory', 'category')) : redirect()->back()->with('error', 'Danh mục không tồn tại!');
        } catch (\Exception $exception) {
            $this->logError($exception);
            return redirect()->back()->with('error', 'Có lỗi xảy ra vui lòng thử lại sau!');
        }

    }

    public function update(CategoryRequest $request, $id)
    {
        try {
            $datas = $request->except(['_token', '_method']);
            $datas['status'] = $request->has('status') ? Category::ACTIVE : Category::INACTIVE;
            $datas['slug'] = Helpers::createSlug($datas['name']);
            Category::where('id', $id)->update($datas);
            return redirect()->route('admin.category.index')->with('success', 'Chỉnh sửa thành công');
        } catch (\Exception $exception) {
            $this->logError($exception);
            return redirect()->back()->withInput()->with('error', 'Chỉnh sửa thất bại');
        }
    }

    public function destroy($id)
    {
        try {
            $datas = Category::where('id', $id)->first();
            if (!$datas) {
                return redirect()->back()->with('error', 'Danh mục không tồn tại');
            }
            $datas->deleted = Category::DELETED;
            $datas->save();
            return redirect()->route('admin.category.index')->with('success','Xóa thành công');
        } catch (\Exception $exception) {
            $this->logError($exception);
            return redirect()->back()->withInput()->with('error', 'xóa thất bại');
        }
    }
}
