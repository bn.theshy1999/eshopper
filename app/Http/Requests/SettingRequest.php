<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
            {
                return [
                    'config_key' => 'required|string|min:1|max:255|unique:settings,config_key,null,id',
                    'config_value' => 'required|string|min:1|max:255',
                ];
            }
            case 'PUT':
            {
                return [
                    'config_key' => 'string|min:1|max:255|unique:settings,config_key,' . $this->segment(4) . ',id',
                    'config_value' => 'nullable|string|min:1|max:255|',
                ];
            }
            default:
                break;
        }
    }
}
