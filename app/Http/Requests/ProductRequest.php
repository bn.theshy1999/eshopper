<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
            {
                return [
                    'name' => 'required|string|min:1|max:255|unique:products,name,null,id',
                    'price' => 'required|numeric|min:1',
                    'feature_image_path' => 'required|mimes:png,jpg|max:2048',
                    'content' => 'string|max:255',
                    'category_id' => 'required|max:255',
                ];
            }
            case 'PUT':
            {
                return [
                    'name' => 'required|string|min:1|max:255|unique:products,name,' . $this->segment(4) . ',id',
                    'price' => 'required|numeric|min:1',
                    'feature_image_path' => 'nullable|mimes:png,jpg|max:2048',
                    'content' => 'nullable|string|max:255',
                    'category_id' => 'nullable',
                ];
            }
            default:
                break;
        }
    }

    public function attributes()
    {
        return [
            'name' => 'Tên sản phẩm',
            'price' => 'Giá bán',
            'feature_image_path' => 'Ảnh sản phẩm',
            'content' => 'Mô tả ngắn',
        ];
    }
}
