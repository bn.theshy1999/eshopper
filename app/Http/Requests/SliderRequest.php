<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SliderRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
            {
                return [
                    'name' => 'required|string|min:1|max:255|unique:sliders,name,null,id',
                    'image_path' => 'required|mimes:png,jpg|max:2048',
                    'description' => 'string|max:255',
                ];
            }
            case 'PUT':
            {
                return [
                    'name' => 'string|min:1|max:255|unique:sliders,name,' . $this->segment(4) . ',id',
                    'image_path' => 'nullable|mimes:png,jpg|max:2048',
                    'description' => 'nullable|string|max:255',
                ];
            }
            default:
                break;
        }
    }
}
