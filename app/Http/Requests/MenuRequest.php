<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MenuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()){
            case 'POST':
            {
                return [
                    'name' => 'required|string|min:1|max:255|unique:menu,name,null,id',
                ];
            }
            case 'PUT':
            {
                return [
                    'name' => 'required|string|min:1|max:255|unique:menu,name,' . $this->segment(4) . ',id',
                ];
            }
        }
    }
    public function attributes()
    {
        return [
            'name' => 'Tên Menu'
        ];
    }
}
