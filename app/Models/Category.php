<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    const ACTIVE = 1;
    const INACTIVE = 0;
    const DELETED = 1;
    const NOT_DELETED = 0;

    protected $table = 'categories';
    protected $fillable = [
        'parent_id',
        'name',
        'slug',
        'status',
        'deleted',
        'created_at',
        'updated_at',
    ];

    public function scopeGetCategory()
    {
        return $this::with(['parent', 'children'])->where('deleted', self::NOT_DELETED);
    }


    public function scopeGetCategoryParent()
    {
        return $this::where('deleted', self::NOT_DELETED)->where('parent_id', 0);
    }


    public function scopeGetCategoryChildren()
    {
        return $this::where('deleted', self::NOT_DELETED)->where('parent_id', '!=', 0);
    }


    public function parent()
    {
        return $this->belongsTo($this, 'parent_id');
    }


    public function getBySlug($slug)
    {
        return self::where(['slug' => $slug, 'status' => self::ACTIVE , 'deleted'=> self::NOT_DELETED])->first();
    }


    public function children()
    {
        return $this->hasMany($this, 'parent_id')->where('status',1);
    }


    public function getById($id)
    {
        return self::where(['id' => $id, 'status' => self::ACTIVE , 'deleted'=> self::NOT_DELETED])->first();
    }

    public function Products()
    {
        return $this->hasMany(Product::class,'category_id');
    }
}
