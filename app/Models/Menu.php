<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menu extends Model
{
    use SoftDeletes;

    use HasFactory;
    protected $table = 'menu';
    protected $fillable = [
        'parent_id',
        'name',
        'slug',
        'deleted',
        'created_at',
        'updated_at',
    ];
    public function scopeGetMenu()
    {
        return $this::with(['parent', 'children']);
    }
    public function parent()
    {
        return $this->belongsTo($this, 'parent_id');
    }
    public function children()
    {
        return $this->hasMany($this, 'parent_id');
    }
}
