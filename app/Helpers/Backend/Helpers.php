<?php

namespace App\Helpers\Backend;

use App\Models\Order;
use App\Models\Setting;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class Helpers
{
    public static function createSlug($str = '')
    {
        $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
        $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
        $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
        $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
        $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
        $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
        $str = preg_replace("/(đ)/", 'd', $str);
        $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
        $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
        $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
        $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
        $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
        $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
        $str = preg_replace("/(Đ)/", 'D', $str);
        $str = preg_replace("/(\“|\”|\‘|\’|\,|\!|\&|\;|\@|\#|\%|\~|\`|\=|\_|\'|\]|\[|\}|\{|\)|\(|\+|\^)/", '-', $str);
        $str = preg_replace("/( )/", '-', $str);
        return strtolower($str);
    }

    public static function uploadImage($request, $keyRequest, $customPath = 'uploads/default')
    {
        if ($request->hasFile($keyRequest)) {
            $photo = $request->file($keyRequest);
            $name = time() . '_' . $photo->getClientOriginalName();
            Storage::putFileAs($customPath, $photo, $name);
            return '/' . $customPath . '/' . $name;
        }
        return false;
    }

    public static function uploadMultiImage($albums, $customPath = 'uploads/default')
    {
        $path = [];
        foreach ($albums as $album) {
            $name = time() . '_' . $album->getClientOriginalName();
            Storage::putFileAs($customPath, $album, $name);
            $path[] = '/' . $customPath . '/' . $name;
        }
        return $path;
    }

    public static function deleteImage($image)
    {
        if (Storage::delete($image)) {
            return true;
        }
        return false;
    }

    public static function currencyFormat($num = 0, $sign = ' VNĐ')
    {
        return number_format($num, 0, '', '.') . $sign;
    }

    public static function sendMail($user_id, $email){
        Log::info('chạy vào đây đi e');
        $order = Order::with('order_detail')->where('user_id', $user_id)->orderBy('id','DESC')->first();
        $order_detail = $order->order_detail;
        $data_email = [
            'order' => $order,
            'order_detail' => $order_detail
        ];
        Mail::send('frontend.mail.mail',$data_email ,function ($message) use ($email){
            $message->from('xedienbaominh1@gmail.com','Xe Điện Bảo Minh');
            $message->to($email,'Đặt hàng thành công');
            $message->subject('Xe Điện Bảo Minh - Thông tin đơn hàng');
        });
        return true;
    }

    public static function ConfigSetting($config_key){
        $setting = Setting::where('config_key',$config_key)->first();
//        dd($setting);
        if(!empty($setting)){
            return $setting->config_value;
        }
        return null;
    }

}
