<?php

namespace App\Components;

use App\Models\Menu;

class MenuRecusive
{
    private $html;

    public function __construct()
    {
        $this->html = '';
    }

    function showCategories($categories, $parent_id = 0, $char = '')
    {
        foreach ($categories as $key => $item) {
            // Nếu là chuyên mục con thì hiển thị
            if ($item['parent_id'] == $parent_id) {
                echo '<tr>';
                echo '<td>';
                echo $char . $item['title'];
                echo '</td>';
                echo '</tr>';

                // Xóa chuyên mục đã lặp
                unset($categories[$key]);

                // Tiếp tục đệ quy để tìm chuyên mục con của chuyên mục đang lặp
                showCategories($categories, $item['id'], $char . '|---');
            }
        }
    }

    public function menuRecusiveindex($parenId = 0, $subMark = '')
    {
        $data = Menu::where('parent_id', $parenId)->get();
        foreach ($data as $key => $dataItem)
        {
            $this->html .= '
            <tr>
                <td>'.$dataItem->id.'</td>
                <td>'.$subMark.$dataItem->name.'</td>
                <td>
                    <a href="'.route('admin.menu.edit',$dataItem->id).' " class="btn btn-outline-warning btn-sm">
                        <i class="fas fa-edit"></i> <span>Sửa</span>
                    </a>
                    <button type="button" class="btn btn-outline-danger btn-sm" onclick="deleteData('.$dataItem->id.')">
                        <i class="fas fa-trash-alt"></i> <span>Xóa</span>
                    </button>
                    <form id="delete-form-{{ $value->id }}" action="'.route('admin.menu.destroy',$dataItem->id).'" method="POST" class="d-none">'
                .csrf_field().
                       '<input type="hidden" name="_method" value="DELETE">
                        </form>
                </td>
            </tr>';
                // Xóa chuyên mục đã lặp
                unset($data[$key]);
                $this->menuRecusiveindex($dataItem->id, $subMark.' --');
//            }
        }
        return $this->html;
    }

    public function menuRecusiveAdd($parenId = 0, $subMark = '')
    {
        $data = Menu::where('parent_id', $parenId)->get();
        foreach ($data as $dataItem) {
            $this->html .= '<option value="' . $dataItem->id . '">' . $subMark . $dataItem->name . '</option>';
            $this->menuRecusiveAdd($dataItem->id, $subMark .'--');
        }
        return $this->html;
    }

    public function menuRecusiveEdit($parenIdMenuEdit, $parenId = 0, $subMark = '')
    {
        $data = Menu::where('parent_id', $parenId)->get();
        foreach ($data as $dataItem) {
            if ($parenIdMenuEdit == $dataItem->id) {
                $this->html .= '<option selected value="' . $dataItem->id . '">' . $subMark . $dataItem->name . '</option>';
            } else {
                $this->html .= '<option value="' . $dataItem->id . '">' . $subMark . $dataItem->name . '</option>';
            }
            $this->menuRecusiveEdit($parenIdMenuEdit, $dataItem->id, $subMark . '--');
        }
        return $this->html;
    }
}
