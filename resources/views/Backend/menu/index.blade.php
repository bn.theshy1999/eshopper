@extends('Backend.layouts.admin')

@section('title')
    Trang chủ
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-4">
                    <div class="col-md-3 col-sm-12">
                        <h1 class=" text-dark">Danh sách menu </h1>
                    </div>
                    <div class="form-group col-md-5 col-sm-12 mt-1">
                        <form method="GET">
                            <div class="row">
                                <div class="col-8">
                                    <input name="name" value="{{$name ?? ''}}" placeholder="Tìm kiếm" type="search" class="form-control">
                                </div>
                                <div class="col-4">
                                    <button type="submit" class="btn btn-outline-info">Tìm kiếm</button>
                                </div>

                            </div>
                        </form>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <a href="{{route('admin.menu.create')}}" class="btn btn-success float-right m-2">Thêm mới menu</a>
                    </div>
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Tên Menu</th>
                                <th scope="col">Hành động</th>
                            </tr>
                            </thead>
                            <tbody>
                            {!! $menusindex !!}
                            </tbody>
                        </table>
                    </div>
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <div class="card-footer clearfix ">
            <ul class="pagination pagination-md m-0 " style="justify-content: center!important;">
                {{$menus->appends(request()->all())->links()}}
            </ul>
        </div>
        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
@endsection
