@extends('Backend.layouts.admin')

@section('title')
    {{isset($menu) ? 'Chỉnh sửa Menu' : 'Thêm mới Menu'}}
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-md-6">
                        <h1 class="m-0 text-dark">{{isset($menu) ? 'Chỉnh sửa Menu' : 'Thêm mới Menu'}} </h1>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <a href="{{route('admin.menu.index')}}" class="btn btn-outline-info float-md-right">
                            <i class="fas fa-arrow-circle-left"></i> <span>Quay về danh sách</span>
                        </a>
                    </div>

                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <form method="POST" action="{{ isset($menu) ? route('admin.menu.update', $menu->id) : route('admin.menu.store')}}" enctype="multipart/form-data" >
                            @csrf
                            @if(isset($menu))
                                @method('PUT')
                            @endif
                            <div class="form-group">
                                <label class="label-required" >Tên menu</label>
                                <input required type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{old('name',isset($menu) ? $menu->name : '')}}"
                                       placeholder="Nhập tên danh mục">
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Chọn menu cha <i style="font-size: 12px">* Bỏ qua nếu là menu cha</i></label>
                                <select class="form-control @error('parent_id') is-invalid @enderror" name="parent_id" id="parent_id">
                                    <option value="0">--Chọn menu cha--</option>
                                    {!! $listMenu!!}
{{--                                    @foreach($listCategory as $item)--}}
{{--                                        <option @if(old('parent_id',isset($category) ? $category->parent_id : '') == $item->id)--}}
{{--                                                    selected @endif value="{{$item->id}}"> {{$item->name}} </option>--}}
{{--                                    @endforeach--}}
                                </select>
                            </div>
                            @error('parent_id')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                            <button type="submit" class="btn btn-primary">{{isset($menu) ? 'Chỉnh sửa' : 'Thêm mới'}}</button>
                        </form>
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
@endsection
@section('js')
    <script>
        $('.select2').select2({
            theme: 'bootstrap4'
        });
        $("input[data-bootstrap-switch]").each(function () {
            $(this).bootstrapSwitch('state', $(this).prop('checked'));
        });
    </script>
@endsection
