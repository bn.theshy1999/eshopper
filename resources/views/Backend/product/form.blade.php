@extends('Backend.layouts.admin')

@section('title')
    {{isset($product) ? 'Chỉnh sửa sản phẩm' : 'Thêm mới sản phẩm'}}
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-md-6">
                        <h1 class="m-0 text-dark">{{isset($product) ? 'Chỉnh sửa sản phẩm' : 'Thêm mới sản phẩm'}} </h1>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <a href="{{route('admin.product.index')}}" class="btn btn-outline-info float-md-right">
                            <i class="fas fa-arrow-circle-left"></i> <span>Quay về danh sách</span>
                        </a>
                    </div>

                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <form method="POST"
                      action="{{ isset($product) ? route('admin.product.update', $product->id) : route('admin.product.store')}}"
                      enctype="multipart/form-data">
                    @csrf
                    @if(isset($product))
                        @method('PUT')
                    @endif
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="{{isset($product) ? '' : 'label-required'}}">Tên sản phẩm
                                    <i style="font-size: 12px;color: #a7abaa;">{{isset($product) ? '* Bỏ qua nếu không chỉnh sửa' : ''}}</i></label>
                                <input required type="text" name="name"
                                       class="form-control @error('name') is-invalid @enderror"
                                       value="{{old('name',isset($product) ? $product->name : '')}}"
                                       placeholder="Nhập tên sản phẩm">
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="{{isset($product) ? '' : 'label-required'}}"
                                       for="exampleFormControlSelect1"> Danh mục <i
                                        style="font-size: 12px;color: #a7abaa;">{{isset($product) ? '* Bỏ qua nếu không chỉnh sửa' : ''}}</i></label>
                                <select class="form-control @error('category_id') is-invalid @enderror"
                                        name="category_id" id="category_id">
                                    <option value="0">--Chọn danh mục--</option>
                                    @foreach($listCategory as $item)
                                        <optgroup label="{{$item->name}}">
                                            @forelse($item->children as $children)
                                                <option
                                                    @if(old('category_id',isset($product) ? $product->category_id : '') == $children->id)
                                                        selected
                                                    @endif value="{{$children->id}}"> {{$children->name}} </option>
                                            @empty
                                            @endforelse
                                        </optgroup>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="{{isset($product) ? '' : 'label-required'}}">Giá sản phẩm
                                    <i style="font-size: 12px;color: #a7abaa;">{{isset($product) ? '* Bỏ qua nếu không chỉnh sửa' : ''}}</i></label>
                                <input {{isset($product) ? '' : 'required'}} type="text" name="price"
                                       class="form-control @error('price') is-invalid @enderror"
                                       value="{{old('price',isset($product) ? $product->price : '')}}"
                                       placeholder="Nhập giá sản phẩm">
                                @error('price')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="{{isset($product) ? '' : 'label-required'}}">Nhập Tag cho sản phẩm
                                    <i style="font-size: 12px;color: #a7abaa;">{{isset($product) ? '* Bỏ qua nếu không chỉnh sửa' : ''}}</i></label>
                                <select required name="tags[]" class="form-control tags_select_choose"
                                        multiple="multiple">
                                    @if(!empty($product))
                                        @foreach($product->tags as $item)
                                            <option value="{{$item->name}}" selected>{{$item->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="{{isset($product) ? '' : 'label-required'}}">Ảnh đại diện
                                    <i style="font-size: 12px;color: #a7abaa;">{{isset($product) ? '* Bỏ qua nếu không chỉnh sửa' : ''}}</i></label>
                                <input {{isset($product) ? '' : 'required'}} type="file" name="feature_image_path"
                                       class="form-control-file @error('feature_image_path') is-invalid @enderror"
                                       value="{{old('feature_image_path')}}"
                                       placeholder="Chọn ảnh đại diện" onchange="readURL(this);">
                                <div class="col-md-4 mt-2">
                                    <div class="row">
                                        <img id="blah" style="width: 100%;height: 200px;"
                                             src = "{{isset($product) ? $product->feature_image_path : asset('Backend/img/placeholder.jpg')}}"
                                        >
                                    </div>
                                </div>
                                @error('feature_image_path')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>
                            <div class="form-group">
                                <label class="{{isset($product) ? '' : 'label-required'}}">Ảnh Chi tiết
                                    <i style="font-size: 12px;color: #a7abaa;">{{isset($product) ? '* Bỏ qua nếu không chỉnh sửa' : ''}}</i></label>
                                <input type="file" multiple name="image_path[]"
                                       class="form-control-file @error('image_path') is-invalid @enderror"
                                       value="{{old('image_path')}}" >
{{--                                <div class="col-md-12 mt-2">--}}
{{--                                    <div class="row">--}}
{{--                                        @if(!empty($product))--}}
{{--                                            @foreach($product->image as $item)--}}
{{--                                                <div class="col-md-3">--}}
{{--                                                    <img style="width: 100%;height: 130px;" src="{{$item->image_path}}">--}}
{{--                                                </div>--}}
{{--                                            @endforeach--}}
{{--                                        @else--}}
{{--                                            <img id="blah22" style="width: 100%;height: 200px;"--}}
{{--                                                 src = "{{asset('Backend/img/placeholder.jpg')}}"--}}
{{--                                            >--}}
{{--                                        @endif--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                                @if(!empty($product))
                                    <div class="col-md-12 mt-2">
                                        <div class="row">
                                            @foreach($product->image as $item)
                                                <div class="col-md-3">
                                                    <img id="blah" style="width: 100%;height: 130px;" src="{{$item->image_path}}">
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="{{isset($product) ? '' : 'label-required'}}">Nhập nội dung
                                    <i style="font-size: 12px;color: #a7abaa;">{{isset($product) ? '* Bỏ qua nếu không chỉnh sửa' : ''}}</i></label>
                                <textarea name="contents" class="ckeditor" rows="3">
                                    {{isset($product) ? $product->content : ''}}
                                </textarea>
                                @error('content')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <button type="submit"
                                class="btn btn-primary">{{isset($product) ? 'Chỉnh sửa' : 'Thêm mới'}}</button>
                    </div>
                </form>
            </div>

            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
    </div>
@endsection
@section('js')
    <script>
        $(function () {
            $('.select2').select2({
                theme: 'bootstrap4'
            });
            $("input[data-bootstrap-switch]").each(function () {
                $(this).bootstrapSwitch('state', $(this).prop('checked'));
            });
            $(".tags_select_choose").select2({
                tags: true,
                tokenSeparators: [',']
            })
        })
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
