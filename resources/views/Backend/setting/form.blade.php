@extends('Backend.layouts.admin')

@section('title')
    {{isset($setting) ? 'Chỉnh sửa setting' : 'Thêm mới setting'}}
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-md-6">
                        <h1 class="m-0 text-dark">{{isset($setting) ? 'Chỉnh sửa setting' : 'Thêm mới setting'}} </h1>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <a href="{{route('admin.setting.index')}}" class="btn btn-outline-info float-md-right">
                            <i class="fas fa-arrow-circle-left"></i> <span>Quay về danh sách</span>
                        </a>
                    </div>

                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <form method="POST"
                      action="{{ isset($setting) ? route('admin.setting.update', $setting->id) : route('admin.setting.store') . '?type=' . request()->type}}"
                      enctype="multipart/form-data">
                    @csrf
                    @if(isset($setting))
                        @method('PUT')
                    @endif
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="{{isset($setting) ? '' : 'label-required'}}">Config Key
                                    <i style="font-size: 12px;color: #a7abaa;">{{isset($setting) ? '* Bỏ qua nếu không chỉnh sửa' : ''}}</i></label>
                                <input required type="text" name="config_key"
                                       class="form-control @error('config_key') is-invalid @enderror"
                                       value="{{old('config_key',isset($setting) ? $setting->config_key : '')}}"
                                       placeholder="Nhập tên sản phẩm">
                                @error('config_key')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            @if(request()->type === 'Text')
                                <div class="form-group">
                                    <label class="{{isset($setting) ? '' : 'label-required'}}">Config Value
                                        <i style="font-size: 12px;color: #a7abaa;">{{isset($setting) ? '* Bỏ qua nếu không chỉnh sửa' : ''}}</i></label>
                                    <input required type="text" name="config_value"
                                           class="form-control @error('config_value') is-invalid @enderror"
                                           value="{{old('config_value',isset($setting) ? $setting->config_value : '')}}"
                                           placeholder="Nhập tên sản phẩm">
                                    @error('config_value')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                            @elseif(request()->type === 'Textarea')
                                        <div class="form-group">
                                            <label class="{{isset($setting) ? '' : 'label-required'}}">Config Value
                                                <i style="font-size: 12px;color: #a7abaa;">{{isset($setting) ? '* Bỏ qua nếu không chỉnh sửa' : ''}}</i></label>
                                            <textarea required name="config_value"
                                                   class="form-control @error('config_value') is-invalid @enderror"
                                                      placeholder="Nhập tên sản phẩm" rows="5">
                                                {{isset($setting) ? $setting->config_value : ''}} </textarea>
                                            @error('config_value')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                            @endif

                            </div>
                        </div>
                    </div>
                    <button type="submit"
                            class="btn btn-primary">{{isset($setting) ? 'Chỉnh sửa' : 'Thêm mới'}}
                    </button>
                </form>
            </div>

            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
    </div>
@endsection
@section('js')
    <script>
        $(function () {
            $('.select2').select2({
                theme: 'bootstrap4'
            });
            $("input[data-bootstrap-switch]").each(function () {
                $(this).bootstrapSwitch('state', $(this).prop('checked'));
            });
            $(".tags_select_choose").select2({
                tags: true,
                tokenSeparators: [',']
            })
        })

    </script>
@endsection
