@extends('Backend.layouts.admin')

@section('title')
    Setting
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-4">
                    <div class="col-md-3 col-sm-12">
                        <h1 class=" text-dark">Danh sách Setting </h1>
                    </div>
                    <div class="form-group col-md-5 col-sm-12 mt-1">
                        <form method="GET">
                            <div class="row">
                                <div class="col-8">
                                    <input name="name" value="{{$name ?? ''}}" placeholder="Tìm kiếm" type="search" class="form-control">
                                </div>
                                <div class="col-4">
                                    <button type="submit" class="btn btn-outline-info">Tìm kiếm</button>
                                </div>

                            </div>
                        </form>
                    </div>
                        <div class="col-md-4 col-sm-12 ">
                            <div class="btn-group float-right">
                                <button type="button" class="btn btn-success float-right mt-2">Action</button>
                                <button type="button" class="btn btn-success dropdown-toggle dropdown-toggle-split mt-2 mr-5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="{{route('admin.setting.create') . '?type=Text'}}">Contact</a>
                                    <a class="dropdown-item" href="{{route('admin.setting.create') . '?type=Textarea'}}">Copy Right</a>
                                </div>
                            </div>
                        </div>
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Config Key</th>
                                <th scope="col">Config Value</th>
                                <th scope="col">Hành động</th>

                            </tr>
                            </thead>
                            <tbody>
                            @forelse($datas as $value)
                                <tr>
                                    <td>{{$value->id}}</td>
                                    <td>{{ $value->config_key }}</td>
                                    <td>{{ $value->config_value }}</td>
                                    <td>
                                        <a href="{{route('admin.setting.edit',$value->id) . '?type=' . $value->type}}" class="btn btn-outline-warning btn-sm">
                                            <i class="fas fa-edit"></i> <span>Sửa</span>
                                        </a>
                                        <button type="button" class="btn btn-outline-danger btn-sm" onclick="deleteData({{$value->id}})">
                                            <i class="fas fa-trash-alt"></i> <span>Xóa</span>
                                        </button>
                                        <form id="delete-form-{{ $value->id }}" action="{{route('admin.setting.destroy',$value->id)}}" method="POST" class="d-none">
                                            @csrf()
                                            @method('DELETE')
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td class="text-center" colspan="5">Không có dữ liệu</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <div class="card-footer clearfix">
            <ul class="pagination pagination-md m-0 " style="justify-content: center!important;">
                {{$datas->appends(request()->all())->links()}}
            </ul>
        </div>
        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
@endsection
