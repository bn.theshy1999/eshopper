@extends('Backend.layouts.admin')

@section('title')
    {{isset($slider) ? 'Chỉnh sửa slider' : 'Thêm mới slider'}}
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-md-6">
                        <h1 class="m-0 text-dark">{{isset($slider) ? 'Chỉnh sửa slider' : 'Thêm mới slider'}} </h1>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <a href="{{route('admin.slider.index')}}" class="btn btn-outline-info float-md-right">
                            <i class="fas fa-arrow-circle-left"></i> <span>Quay về danh sách</span>
                        </a>
                    </div>

                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <form method="POST"
                      action="{{ isset($slider) ? route('admin.slider.update', $slider->id) : route('admin.slider.store')}}"
                      enctype="multipart/form-data">
                    @csrf
                    @if(isset($slider))
                        @method('PUT')
                    @endif
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="{{isset($slider) ? '' : 'label-required'}}">Tên sản phẩm
                                    <i style="font-size: 12px;color: #a7abaa;">{{isset($slider) ? '* Bỏ qua nếu không chỉnh sửa' : ''}}</i></label>
                                <input required type="text" name="name"
                                       class="form-control @error('name') is-invalid @enderror"
                                       value="{{old('name',isset($slider) ? $slider->name : '')}}"
                                       placeholder="Nhập tên sản phẩm">
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="{{isset($slider) ? '' : 'label-required'}}">Hình ảnh
                                    <i style="font-size: 12px;color: #a7abaa;">{{isset($slider) ? '* Bỏ qua nếu không chỉnh sửa' : ''}}</i></label>
                                <input {{isset($slider) ? '' : 'required'}} type="file" name="image_path"
                                       class="form-control-file @error('image_path') is-invalid @enderror"
                                       value="{{old('image_path')}}"
                                       placeholder="Chọn ảnh đại diện" onchange="readURL(this);">
                                <div class="col-md-4 mt-2">
                                    <div class="row">
                                        <img id="blah" style="width: 100%;height: 200px;"
                                        src = "{{isset($slider) ? $slider->image_path : asset('Backend/img/placeholder.jpg')}}"
                                        >
                                    </div>
                                </div>
                                @error('image_path')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>
                            <div class="form-group">
                                <label class="{{isset($slider) ? '' : 'label-required'}}">Mô tả Slider
                                    <i style="font-size: 12px;color: #a7abaa;">{{isset($slider) ? '* Bỏ qua nếu không chỉnh sửa' : ''}}</i></label>
                                <textarea name="description" class="ckeditor" rows="3">
                                    {{isset($slider) ? $slider->description : ''}}
                                </textarea>
                                @error('description')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div>
                            <button type="submit"
                                    class="btn btn-primary">{{isset($slider) ? 'Chỉnh sửa' : 'Thêm mới'}}
                            </button>
                        </div>
                    </div>

                </form>
            </div>

            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
    </div>
@endsection
@section('js')
    <script>
        $(function () {
            $('.select2').select2({
                theme: 'bootstrap4'
            });
            $("input[data-bootstrap-switch]").each(function () {
                $(this).bootstrapSwitch('state', $(this).prop('checked'));
            });
            $(".tags_select_choose").select2({
                tags: true,
                tokenSeparators: [',']
            })
        })
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
