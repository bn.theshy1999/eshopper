<div class="left-sidebar">
    <h2>Category</h2>
    <div class="panel-group category-products" id="accordian"><!--category-productsr-->
        @foreach($category as $itemcategory)
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    @if($itemcategory->children->count())
                    <a data-toggle="collapse" data-parent="#accordian" href="#sportswear_{{$itemcategory->id}}">
                        <span class="badge pull-right">
                            <i class="fa fa-plus"></i>
                        </span>
                        {{$itemcategory->name}}
                    </a>
                    @else
                        <a href="#">
                        <span class="badge pull-right">
                        </span>
                            {{$itemcategory->name}}
                        </a>
                    @endif
                </h4>
            </div>
{{--            {{dd($itemcategory->children)}}--}}
            <div id="sportswear_{{$itemcategory->id}}" class="panel-collapse collapse">
                <div class="panel-body">
                    <ul>
                        @foreach($itemcategory->children as $categorychildren)
                        <li><a href="{{route('.category.product',['slug'=>$categorychildren->slug,'id'=>$categorychildren->id])}}">{{$categorychildren->name}} </a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        @endforeach
    </div><!--/category-products-->


</div>
