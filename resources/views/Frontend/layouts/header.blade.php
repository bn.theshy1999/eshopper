<header id="header"><!--header-->
    <div class="header_top"><!--header_top-->
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="contactinfo">
                        <ul class="nav nav-pills">
                            <li><a href="#"><i class="fa fa-phone">

                                    </i> {{\App\Helpers\Backend\Helpers::ConfigSetting('contact phone')}}</a>
                            </li>
                            <li><a href="#"><i class="fa fa-envelope"></i>
                                    {{\App\Helpers\Backend\Helpers::ConfigSetting('email')}}
                                </a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="social-icons pull-right">
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="{{\App\Helpers\Backend\Helpers::ConfigSetting('facebook')}}">
                                    <i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header_top-->

    <div class="header-middle"><!--header-middle-->
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="logo pull-left">
                        <a href="{{route('.index')}}"><img src="{{asset('Eshopper/images/home/logo.png')}}" alt=""/></a>
                    </div>
{{--                    <div class="btn-group pull-right">--}}
{{--                        <div class="btn-group">--}}
{{--                            <button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">--}}
{{--                                USA--}}
{{--                                <span class="caret"></span>--}}
{{--                            </button>--}}
{{--                            <ul class="dropdown-menu">--}}
{{--                                <li><a href="#">Canada</a></li>--}}
{{--                                <li><a href="#">UK</a></li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}

{{--                        <div class="btn-group">--}}
{{--                            <button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">--}}
{{--                                DOLLAR--}}
{{--                                <span class="caret"></span>--}}
{{--                            </button>--}}
{{--                            <ul class="dropdown-menu">--}}
{{--                                <li><a href="#">Canadian Dollar</a></li>--}}
{{--                                <li><a href="#">Pound</a></li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
                <div class="col-sm-8">
                    <div class="shop-menu pull-right">
                        <ul class="nav navbar-nav">
                            <li><a href="#"><i class="fa fa-user"></i> Account</a></li>
                            <li><a href="#"><i class="fa fa-star"></i> Wishlist</a></li>
                            <li><a href="checkout.html"><i class="fa fa-crosshairs"></i> Checkout</a></li>
                            <li><a href="cart.html"><i class="fa fa-shopping-cart"></i> Cart</a></li>
                            <li><a href="login.html"><i class="fa fa-lock"></i> Login</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header-middle-->

    <div class="header-bottom"><!--header-bottom-->
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse"
                                data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="mainmenu pull-left">
                        <ul class="nav navbar-nav collapse navbar-collapse">
                            <li><a href="{{route('.index')}}" class="active">Home</a></li>
                            @foreach($categoryMenu as $categoryMenuItem)
                                @if($categoryMenuItem->children->count())
                            <li class="dropdown"><a href="#">{{$categoryMenuItem->name}}<i class="fa fa-angle-down"></i></a>
                                <ul role="menu" class="sub-menu">
                                    @foreach($categoryMenuItem->children as $Itemchildren)
                                    <li><a href="{{route('.category.product',['slug'=>$Itemchildren->slug,'id'=>$Itemchildren->id])}}">{{$Itemchildren->name}}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                                @else
                            <li class="dropdown"><a href="#">{{$categoryMenuItem->name}}</a>
                            </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="search_box pull-right">
                        <input type="text" placeholder="Search"/>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header-bottom-->
</header><!--/header-->
