@extends('Frontend.layouts.master')

@section('title')
    Product
@endsection


@section('content')
    <section id="advertisement">
        <div class="container">
            <img src="images/shop/advertisement.jpg" alt="" />
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    @include('Frontend.layouts.sidebar')
                </div>

                <div class="col-sm-9 padding-right">
                    <div class="features_items"><!--features_items-->
                        <h2 class="title text-center">Features Items</h2>
                        @forelse($product as $item)
                        <div class="col-sm-4">
                            <div class="product-image-wrapper">
                                <div class="single-products">
                                    <div class="productinfo text-center">
                                        <img src="{{asset($item->feature_image_path)}}" alt="" />
                                        <h2>{{number_format($item->price)}}</h2>
                                        <p>{{$item->name}}</p>
                                        <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                    </div>
                                    <div class="product-overlay">
                                        <div class="overlay-content">
                                            <h2>{{number_format($item->price)}}</h2>
                                            <p>{{$item->name}}</p>
                                            <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="choose">
                                    <ul class="nav nav-pills nav-justified">
                                        <li><a href=""><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                                        <li><a href=""><i class="fa fa-plus-square"></i>Add to compare</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        @empty
                            <p>Không có sản phẩm</p>
                        @endforelse
                        <div class="col-sm-12">
                            <ul class="pagination">
                                {{$product->appends(request()->all())->links()}}
                            </ul>
                        </div>
                    </div><!--features_items-->
                </div>
            </div>
        </div>
    </section>
@endsection
