<div class="category-tab">
    <div class="col-sm-12">
        <ul class="nav nav-tabs">
            @foreach($category as $key => $categoryItem)
                @if($key < 8)
            <li class="{{$key == 0 ? 'active' : ''}}"><a href="#tshirt_{{$categoryItem->id}}" data-toggle="tab">
                   {{$categoryItem->name}}
                </a></li>
                @endif
            @endforeach
        </ul>
    </div>
    <div class="tab-content">
        @foreach($category as $key => $Item)
        <div class="tab-pane fade {{$key == 0 ? 'active in' : ''}}" id="tshirt_{{$Item->id}}">
{{--            {{dd($Item)}}--}}
            @forelse($Item->children as $ItemProductTab)
                @foreach($ItemProductTab->Products as $value)
{{--                {{dd($value)}}--}}
            <div class="col-sm-3">
                <div class="product-image-wrapper">
                    <div class="single-products">
                        <div class="productinfo text-center">
                            <img src="{{asset($value->feature_image_path)}}" alt=""/>
                            <h2>{{number_format($value->price)}}</h2>
                            <p>{{$value->name}}</p>
                            <a href="#" class="btn btn-default add-to-cart"><i
                                    class="fa fa-shopping-cart"></i>Thêm vào giỏ hàng</a>
                        </div>

                    </div>
                </div>
            </div>
            @endforeach
            @empty
                <p>Không có sản phẩm</p>
            @endforelse
        </div>
        @endforeach
    </div>
</div>
