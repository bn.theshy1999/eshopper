@extends('Frontend.layouts.master')

@section('title')
    Home page
@endsection

@section('css')

@endsection

@section('content')

@include('Frontend.home.slider')
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    @include('Frontend.layouts.sidebar')
                </div>

                <div class="col-sm-9 padding-right">
                    <!--features_items-->
                    @include('Frontend.home.feature_product')
                   <!--features_items-->

                    <!--/category-tab-->
                    @include('Frontend.home.category_tab')
                    <!--/category-tab-->

                    <!--recommended_items-->
                    @include('Frontend.home.recommend_product')
                   <!--/recommended_items-->

                </div>
            </div>
        </div>
    </section>

@endsection
