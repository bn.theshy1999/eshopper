<?php

use App\Http\Controllers\Backend\AdminController;
use App\Http\Controllers\Backend\CategoryController;
use App\Http\Controllers\Backend\MenuController;
use App\Http\Controllers\Backend\ProductController;
use App\Http\Controllers\Backend\SettingController;
use App\Http\Controllers\Backend\SliderController;
use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\CategoryControllerList;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::prefix('admin')->name('admin.')->group(function () {
    Route::get('/', [AdminController::class, 'index'])->name('index');
    Route::get('login', [AdminController::class, 'login'])->name('login');
    Route::post('post_login', [AdminController::class, 'postlogin'])->name('post_login');



    Route::get('category', [CategoryController::class, 'index'])->name('category.index');
    Route::get('category/create', [CategoryController::class, 'create'])->name('category.create');
    Route::post('category/store', [CategoryController::class, 'store'])->name('category.store');
    Route::get('category/edit/{id}', [CategoryController::class, 'edit'])->name('category.edit');
    Route::put('category/update/{id}', [CategoryController::class, 'update'])->name('category.update');
    Route::delete('category/destroy/{id}', [CategoryController::class, 'destroy'])->name('category.destroy');

    Route::get('menu', [MenuController::class, 'index'])->name('menu.index');
    Route::get('menu/create', [MenuController::class, 'create'])->name('menu.create');
    Route::post('menu/store', [MenuController::class, 'store'])->name('menu.store');
    Route::get('menu/edit/{id}', [MenuController::class, 'edit'])->name('menu.edit');
    Route::put('menu/update/{id}', [MenuController::class, 'update'])->name('menu.update');
    Route::delete('menu/destroy/{id}', [MenuController::class, 'destroy'])->name('menu.destroy');

    Route::get('product', [ProductController::class, 'index'])->name('product.index');
    Route::get('product/create', [ProductController::class, 'create'])->name('product.create');
    Route::post('product/store', [ProductController::class, 'store'])->name('product.store');
    Route::get('product/edit/{id}', [ProductController::class, 'edit'])->name('product.edit');
    Route::put('product/update/{id}', [ProductController::class, 'update'])->name('product.update');
    Route::delete('product/destroy/{id}', [ProductController::class, 'destroy'])->name('product.destroy');

    Route::get('slider', [SliderController::class, 'index'])->name('slider.index');
    Route::get('slider/create', [SliderController::class, 'create'])->name('slider.create');
    Route::post('slider/store', [SliderController::class, 'store'])->name('slider.store');
    Route::get('slider/edit/{id}', [SliderController::class, 'edit'])->name('slider.edit');
    Route::put('slider/update/{id}', [SliderController::class, 'update'])->name('slider.update');
    Route::delete('slider/destroy/{id}', [SliderController::class, 'destroy'])->name('slider.destroy');

    Route::get('setting', [SettingController::class, 'index'])->name('setting.index');
    Route::get('setting/create', [SettingController::class, 'create'])->name('setting.create');
    Route::post('setting/store', [SettingController::class, 'store'])->name('setting.store');
    Route::get('setting/edit/{id}', [SettingController::class, 'edit'])->name('setting.edit');
    Route::put('setting/update/{id}', [SettingController::class, 'update'])->name('setting.update');
    Route::delete('setting/destroy/{id}', [SettingController::class, 'destroy'])->name('setting.destroy');
});

//Frontend
Route::prefix('/')->name('.')->group(function (){
    Route::get('/', [HomeController::class, 'index'])->name('index');
    Route::get('category/{slug}/{id}',[CategoryControllerList::class,'category'])->name('category.product');
});
